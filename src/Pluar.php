<?php

namespace nalofree\utils;

/**
 *
 */
class Pluar
{

  /**
   * @param $n
   * @param $words
   * @return mixed
   */
  public static function getRu($n, $words): mixed
  {
    $n = (int)$n;
    return ($n % 10 == 1 && $n % 100 != 11) ? $words[0] : ($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20) ? $words[1] : $words[2]);
  }

  /**
   * @param $n
   * @param $words
   * @return mixed
   */
  public static function getEn($n, $words): mixed
  {
    $n = (int)$n;
    return ($n == 1) ? $words[0] : $words[1];
  }
}